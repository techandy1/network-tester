/**
 * Filename: NetworkTester.java
 * Written By: Andrew Armstrong
 * Written On: 4 October 2016
 * 
 * This program allows a user to enter in the number of times to ping a ip address
 * or website to test their Internet connection.
 * 
 * */
package networktester;

import java.io.*;
import java.util.*;

public class NetworkTester {
	
	public static void main(String[] args) throws IOException {
		
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		final String pingStr = "ping";
		final String pingCounterArg = "-c";		
		
		NetworkTester ping = new NetworkTester();
		
		ArrayList<String> commands = new ArrayList<String>();
		
		commands.add(pingStr);
	    commands.add(pingCounterArg);
	    
	    System.out.println("Please enter the number of pings you would like: (number) ");
		String pingCount = input.nextLine();
	    commands.add(pingCount);
	    
	    System.out.println("Please enter URL (www.google.com) or IP Address (127.0.0.1): ");
		String pingAddress = input.nextLine();
	    commands.add(pingAddress);
	    
	    ping.doCommand(commands);
	}
	
	public void doCommand(ArrayList<String> command) throws IOException {
		
		String s = null;
		ProcessBuilder pb = new ProcessBuilder(command);
		Process process = pb.start();
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		
		// read the output from the command
		while ((s = stdInput.readLine()) != null) {
			System.out.println(s);
		}

		// read any errors from the attempted command
		if ((s = stdError.readLine()) != null) {
			System.out.println("Please check your input.");
		}
	}
}